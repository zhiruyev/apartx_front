import React,{useEffect} from 'react';
import { useAppDispatch } from '../../hooks/useRedux';
import useRequest from '../../hooks/useRequest';
import { setEmail, setIsCodeSent, setUuidReg } from '../../redux/reducers/LoginPageSlice';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import MainButton from "../UI/MainButton";
import { useAppSelector } from '../../hooks/useRedux';

const RegStepOne = () => {
  const dispatch = useAppDispatch();

  const BASE_URL_REG = 'http://206.189.61.25:8001/apartx_reg/';

  const { responseData, error, sendRequest, responseStatus, isLoading } = useRequest();

  const schema = yup.object().shape({
    email: yup.string()
      .email('Введите правильный адрес электронной почты')
      .required('Поле "Email" обязательно для заполнения'),
  });

  const { register, getValues, trigger, setError, handleSubmit, formState: { errors } } = useForm({
    resolver: yupResolver(schema),
  });

  const { isCodeSent, isCodeVerified, isRegistered } = useAppSelector((state) => state.loginPage);

  async function getCode() {
    const emailValue = getValues('email');

    const isValid = await trigger('email'); // Запуск валидации поля "email"

    if (isValid) {
      const article = { email: emailValue };
      const url = `${BASE_URL_REG}registration/registration/init/`;
      sendRequest(url, article);
    }
  }

  useEffect(() => {
    if (responseStatus === 200 && responseData && responseData.uuid) {
      const emailValue = getValues('email');
      dispatch(setUuidReg(responseData.uuid));
      dispatch(setEmail(emailValue));
      dispatch(setIsCodeSent(true));
    }
  }, [responseStatus, responseData, dispatch, getValues]);

  if (isLoading) {
    return <div className="custom-loader"></div>;
  }

  return (
    <>
      <label>Email</label>
      <input type="text" {...register('email')} />
      {errors.email && <p>{errors.email.message}</p>}
      {error && error.response && error.response.data.detail}
      <MainButton onClick={getCode}>Получить код</MainButton>
    </>
  )
}

export default RegStepOne;
