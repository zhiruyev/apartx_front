import React, {useEffect,  useState} from 'react';
import {useAppSelector} from '../../hooks/useRedux';
import {useTranslation} from "react-i18next";
import RegStepOne from "./RegStepOne";
import RegStepTwo from "./RegStepTwo";
import RegStepThree from "./RegStepThree";

const FormReg = () => {
    const { t } = useTranslation();

    const { isCodeSent, isCodeVerified, isRegistered } = useAppSelector((state) => state.loginPage);

    const [isCodeSentValue, setIsCodeSentValue] = useState<boolean>(false);

    useEffect(() => {
        setIsCodeSentValue(isCodeSent);
    }, [isCodeSent]);

    localStorage.clear();

    return (
        <div>
            <h1>{t('auth.register')}</h1>
            <form>
                {!isCodeSent && <RegStepOne />}
                {isCodeSent && !isCodeVerified && <RegStepTwo />}
                {isCodeVerified && <RegStepThree />}
            </form>
        </div>
    );
};

export default FormReg;

