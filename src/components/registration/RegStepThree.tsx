import React, {useState, useEffect} from 'react'
import MainButton from "../UI/MainButton";
import useRequest from "../../hooks/useRequest";
import * as yup from "yup";
import {useAppSelector} from '../../hooks/useRedux';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';


import {useNavigate} from "react-router-dom";

const RegStepThree = () => {
    const navigate = useNavigate();
    const {uuidReg} = useAppSelector((state) => state.loginPage);
    const BASE_URL_REG = 'http://206.189.61.25:8001/apartx_reg/';

    const {responseData, error, sendRequest, responseStatus, isLoading} = useRequest();

    const schema = yup.object().shape({
        password: yup.string()
            .required('Поле "Пароль" обязательно для заполнения')
            .min(8, 'Пароль должен содержать минимум 8 символов')
            .matches(/[a-zA-Z]/, 'Пароль должен содержать как минимум 1 букву на латинском языке'),
        confirmPassword: yup.string()
            .oneOf([yup.ref('password'), undefined], 'Пароли должны совпадать')
            .nullable()
            .required('Поле "Подтверждение пароля" обязательно для заполнения'),
    });

    const {register, getValues, trigger, setError, handleSubmit, formState: {errors}} = useForm({
        resolver: yupResolver(schema),
    });

    const [showPassword, setShowPassword] = useState(false);
    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    async function handleReg() {
        const passwordValue = getValues("password");
        const isValid = await trigger('password');
        if (isValid) {
            const article = {password: passwordValue};
            const url = `${BASE_URL_REG}registration/registration/${uuidReg}/finish/`;
            sendRequest(url, article);
          
        }
    }
    useEffect(() => {
        if (responseStatus === 200 && responseData && responseData.uuid) {
            navigate('/login');
       
        }
      }, [responseStatus, responseData]);
    
    if (isLoading) {
        return <div className="custom-loader"></div>;
      }
    
    return (
        <>
            <label>Пароль</label>
            <div className={"input_show_password"}>
                <input type={showPassword ? 'text' : 'password'} {...register('password')} />
                <button type="button" className={showPassword ? "active" : ""}
                        onClick={togglePasswordVisibility}>
                </button>
            </div>
            {errors.password && <p>{errors.password.message}</p>}
            <label>Подтверждение пароля</label>
            <input type={showPassword ? 'text' : 'password'} {...register('confirmPassword')} />
            {errors.confirmPassword && <p>{errors.confirmPassword.message}</p>}
            {error && error.response && error.response.data.detail}
            <MainButton onClick={handleReg}>Зарегистрироваться</MainButton>
        </>
    )
}

export default RegStepThree
