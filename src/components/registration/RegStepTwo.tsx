import React,{useEffect} from 'react';
import {useAppDispatch, useAppSelector} from '../../hooks/useRedux';
import * as yup from 'yup';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import useRequest from "../../hooks/useRequest";
import MainButton from "../UI/MainButton";
import {setIsCodeVerified} from "../../redux/reducers/LoginPageSlice";

const RegStepTwo = () => {
    const dispatch = useAppDispatch();
    const {uuidReg} = useAppSelector((state) => state.loginPage);
    const BASE_URL_REG = 'http://206.189.61.25:8001/apartx_reg/';


    const {responseData, error, sendRequest, responseStatus, isLoading} = useRequest();

    const schema = yup.object().shape({
        verificationCode: yup.string()
            .required('Поле "Код подтверждения" обязательно для заполнения')
            .min(6, 'Код подтверждения должен содержать минимум 6 символов'),
    });

    const {register, getValues, trigger, setError, handleSubmit, formState: {errors}} = useForm({
        resolver: yupResolver(schema),
    });

    async function handleSubmitCode() {
        const verificationCodeValue = getValues("verificationCode");
        const isValid = await trigger('verificationCode'); // Запуск валидации поля "verificationCode"

        if (isValid) {
            const article = {code: verificationCodeValue};
            const url = `${BASE_URL_REG}registration/registration/${uuidReg}/confirm_email/`;
            sendRequest(url, article);
        }
    }
    useEffect(() => {
        if (responseStatus === 200) {
            dispatch(setIsCodeVerified(true))
        }
    }, [responseStatus, responseData, dispatch, getValues])
    
    if (isLoading) {
        return <div className="custom-loader"></div>;
      }
    

    return (
        <>
            <label>Код подтверждения</label>
            <input type="text" {...register('verificationCode')} />
            {errors.verificationCode && <p>{errors.verificationCode.message}</p>}
            {error && error.response && error.response.data.detail}
            <MainButton onClick={handleSubmitCode}>Подтвердить код</MainButton>
        </>
    )
}

export default RegStepTwo
