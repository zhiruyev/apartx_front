import React from 'react'
import Header from "../components/UI/Header";
import FormReg from "../components/registration/FormReg";

const RegPage = () => {
    return (
        <div>
            <Header></Header>
            <FormReg/>
        </div>
    )
}

export default RegPage
