import {createSlice, PayloadAction} from "@reduxjs/toolkit";

import {ILoginPage} from "../../models/ILoginPage";

const initialState: ILoginPage = {
    email: "",
    access_token: "",
    refresh_token: "",
    username: "",
    is_banned: false,
    uuidReg: "",
    isCodeSent: false,
    isCodeVerified: false,
    isRegistered: false,


};

const loginPageSlice = createSlice({
    name: "loginPage",
    initialState,
    reducers: {
        setEmail: (state, action: PayloadAction<string>) => {
            state.email = action.payload;
        },
        setAccess_token: (state, action: PayloadAction<string>) => {
            state.access_token = action.payload;
        },
        setRefresh_token: (state, action: PayloadAction<string>) => {
            state.refresh_token = action.payload;
        },
        setUsername: (state, action: PayloadAction<string>) => {
            state.username = action.payload;
        },
        setIs_banned: (state, action: PayloadAction<boolean>) => {
            state.is_banned = action.payload;
        },
        setUuidReg: (state, action: PayloadAction<string>) => {
            state.uuidReg = action.payload;
        },
        setIsCodeSent: (state, action: PayloadAction<boolean>) => {
            state.isCodeSent = action.payload;
        },
        setIsCodeVerified: (state, action: PayloadAction<boolean>) => {
            state.isCodeVerified = action.payload;
        },
        setIsRegistered: (state, action: PayloadAction<boolean>) => {
            state.isRegistered = action.payload;
        },
    },
});

export const {
    setEmail,
    setUuidReg,
    setIsCodeSent,
    setIsRegistered,
    setIsCodeVerified,
    setAccess_token,
    setRefresh_token,
    setUsername,
    setIs_banned
} =
    loginPageSlice.actions;
export default loginPageSlice.reducer;